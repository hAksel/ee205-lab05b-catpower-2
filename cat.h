/////////////////////////////////////////////////////////////////////////////
//
///         University of Hawaii, College of Engineering
/// @brief  Lab 05b - CatPower 2 - EE 205 - Spr 2022
///
/// @file cat.h
/// @version 1.0
///
/// @author Aksel Sloan <aksel@hawaii.edu> 
/// @date   14_Feb_2022
/////////////////////////////////////////////////////////////////////////////
#pragma once

const double   CAT_OVER_JOULE = 0;
const char     CAT_POWER = 'c';

extern double fromCatPowerToJoule( double catPower );

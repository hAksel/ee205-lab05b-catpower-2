###############################################################################
###
### @brief
###
### @file
### @version 1.0 - Initial version
###
### Build and test an energy unit conversion program
###
### @author  Aksel Sloan <aksel@hawaii.edu>  
### @date  	 14 February 2022 
###
### @see     https://www.gnu.org/software/make/manual/make.html
###############################################################################
CC     = g++
CFLAGS = -g -Wall -Wextra
TARGET = catPower
all: $(TARGET)


cat.o: cat.cpp cat.h #cat.o depends on cat.cpp and cat.h 
	$(CC) $(CFLAGS) -c cat.cpp #g++ -g -W... -c cat.cpp (what to compile) 

ev.o: ev.cpp ev.h
	$(CC) $(CFLAGS) -c ev.cpp

foe.o: foe.cpp foe.h
	$(CC) $(CFLAGS) -c foe.cpp

gg.o: gg.cpp gg.h
	$(CC) $(CFLAGS) -c gg.cpp

megaton.o: megaton.cpp megaton.h
	$(CC) $(CFLAGS) -c megaton.cpp 


catPower.o: catPower.cpp cat.h ev.h foe.h gg.h megaton.h #catPower.o (compiled .c file called the object file) depends on all the header files  
	$(CC) $(CFLAGS) -c catPower.cpp
catPower: catPower.o cat.o ev.o foe.o gg.o megaton.o
	$(CC) $(CFLAGS) -o $(TARGET) catPower.o cat.o ev.o foe.o gg.o megaton.o

test: catPower
	@./catPower 3.14 j j |  grep -q "3.14 j is 3.14 j"
	@./catPower 3.14 j e |  grep -q "3.14 j is 1.95983E+19 e"
	@./catPower 3.14 j m |  grep -q "3.14 j is 7.50478E-16 m"
	@./catPower 3.14 j g |  grep -q "3.14 j is 2.58862E-08 g"
	@./catPower 3.14 j f |  grep -q "3.14 j is 3.14E-24 f"
	@./catPower 3.14 j c |  grep -q "3.14 j is 0 c"
	@./catPower 3.14 j x |& grep -q "Error number 428"
	
	@./catPower 3.14 m j |  grep -q "3.14 m is 1.31378E+16 j"
	@./catPower 3.14 g e |  grep -q "3.14 g is 2.37729E+27 e"
	@./catPower 3.14 f m |  grep -q "3.14 f is 7.50478E+08 m"
	@./catPower 3.14 c g |  grep -q "3.14 c is 0 g"
	@./catPower |& grep -q "Error number 421"

	@echo "All tests pass"

clean:
	rm -f $(TARGET) *.o



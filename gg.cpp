///////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 05b - CatPower 2 - EE 205 - Spr 2022
///
/// @file gg.cpp
/// @version 1.0
///
/// @author Aksel Sloan <aksel@hawaii.edu>
/// @date   14_Feb_2022
///////////////////////////////////////////////////////////////////////////////

#include "gg.h"

double fromGasToJoule (double gas ) {
   return gas / GAS_OVER_JOULE;
}

double fromJouleToGas (double joule) {
   return joule * GAS_OVER_JOULE;
}


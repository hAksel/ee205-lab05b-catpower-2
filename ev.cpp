///////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 05b - CatPower 2 - EE 205 - Spr 2022
///
/// @file ev.cpp
/// @version 1.0
///
/// @author Aksel Sloan <aksel@hawaii.edu>
/// @date   14_Feb_2022
///////////////////////////////////////////////////////////////////////////////

#include "ev.h"


double fromElectronVoltsToJoule( double electronVolts ) {
   return electronVolts / EV_OVER_JOULE;
}


double fromJouleToElectronVolts( double joule ) {
   return joule * EV_OVER_JOULE;
}

/////////////////////////////////////////////////////////////////////////////
//
///         University of Hawaii, College of Engineering
/// @brief  Lab 05b - CatPower 2 - EE 205 - Spr 2022
///
/// @file megaton.h
/// @version 1.0
///
/// @author Aksel Sloan <aksel@hawaii.edu> 
/// @date   14_Feb_2022
/////////////////////////////////////////////////////////////////////////////
#pragma once

const double   MEGATON_OVER_JOULE = (1 / 4.184e15 );
const char     MEGATON_TNT = 'm';

extern double fromMegatonToJoule( double megaton );

extern double fromJouleToMegaton( double joule );


/////////////////////////////////////////////////////////////////////////////
//
///         University of Hawaii, College of Engineering
/// @brief  Lab 05b - CatPower 2 - EE 205 - Spr 2022
///
/// @file gg.h
/// @version 1.0
///
/// @author Aksel Sloan <aksel@hawaii.edu> 
/// @date   14_Feb_2022
/////////////////////////////////////////////////////////////////////////////
#pragma once

const double   GAS_OVER_JOULE = 8.244e-9;
const char     GALLON_GAS = 'g';

extern double fromGasToJoule (double gas );

extern double fromJouleToGas (double joule); 


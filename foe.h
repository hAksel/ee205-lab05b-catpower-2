/////////////////////////////////////////////////////////////////////////////
//
///         University of Hawaii, College of Engineering
/// @brief  Lab 05b - CatPower 2 - EE 205 - Spr 2022
///
/// @file foe.h
/// @version 1.0
///
/// @author Aksel Sloan <aksel@hawaii.edu> 
/// @date   14_Feb_2022
/////////////////////////////////////////////////////////////////////////////
#pragma once

const double   FOE_OVER_JOULE = 1e-24;
const char     FOE = 'f';


extern double fromFoeToJoule (double foe ); 

extern double fromJouleToFoe (double joule); 

